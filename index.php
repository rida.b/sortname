<?php

require 'Sortname.php';

$sn = new Sortname();

$isiFiles = $sn->readFile('unsorted-names-list.txt');

$splitedNames = [];

foreach ($isiFiles as $nama) {
	array_push($splitedNames, $sn->splitName($nama));
}

$sorted_names = $sn->sortArray($splitedNames);

$write_names = [];

foreach ($sorted_names as $name) {
	$fullname = $name['first_name'] . ' ' . $name['last_name'];
	array_push($write_names, $fullname);
	print_r($fullname);
	print_r('<br/>');
}

$sn->writeFile($write_names, 'sorted-names-list.txt');
