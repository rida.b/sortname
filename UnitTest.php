<?php

use PHPUnit\Framework\TestCase;

require_once "Sortname.php";

class UnitTest extends TestCase
{
    // ======================================================================== TEST CASE READ FILE
    public function test_ReadFileSuccess()
    {
        // Fungsi tes untuk melakukan tes jika file yang di baca sukses, maka ekspektasinya adalah panjang array nya sesuai dengan jumlah line yang ada di dalam file
        // melakukan proses pembacaan kemudian di lakukan verifikasi bahwa panjang dari hasil return adalah 5
        $sn = new Sortname();
        $readFile = $sn->readFile('test-file.txt');
        $this->assertEquals(5, sizeof($readFile));
    }

    public function test_ReadFileFailed()
    {
        // Fungsi tes untuk melakukan tes jika file yang di baca gagal, maka ekspektasinya adalah panjang array nya yang di return adalah 0
        // melakukan proses pembacaan kemudian di lakukan verifikasi bahwa panjang dari hasil return ada 0
        $sn = new Sortname();
        $readFile = $sn->readFile('read-not-exists.txt');
        $this->assertEquals(0, sizeof($readFile));
    }

    // ======================================================================== TEST CASE WRITE FILE
    public function test_WriteFileSuccess()
    {
        $sn = new Sortname();

        $datas = [
            'Tulis Line Ke-1',
            'Tulis Line Ke-2',
            'Tulis Line Ke-3',
            'Tulis Line Ke-4'
        ];

        $outputFileName = 'output-test-file.txt';
        $sn->writeFile($datas, $outputFileName);

        $readFile = $sn->readFile($outputFileName);
        $this->assertEquals(4, sizeof($readFile));

        unlink($outputFileName);
    }

    // ======================================================================== TEST CASE SPLIT NAME
    public function test_SplitNameOne()
    {
        $sn = new Sortname();

        $name = "Rida";

        $splitedName = $sn->splitName($name);

        $this->assertEquals('', $splitedName['first_name']);
        $this->assertEquals('Rida', $splitedName['last_name']);
    }

    public function test_SplitNameTwo()
    {
        $sn = new Sortname();

        $name = "Rida Bintara";

        $splitedName = $sn->splitName($name);

        $this->assertEquals('Rida', $splitedName['first_name']);
        $this->assertEquals('Bintara', $splitedName['last_name']);
    }

    public function test_SplitNameThree()
    {
        $sn = new Sortname();

        $name = "Rida Bintara Anggara";

        $splitedName = $sn->splitName($name);

        $this->assertEquals('Rida Bintara', $splitedName['first_name']);
        $this->assertEquals('Anggara', $splitedName['last_name']);
    }

    // ======================================================================== TEST CASE SORTIR
    public function test_SortName()
    {
        $sn = new Sortname();

        $unsortedSplittedName = [
            ['first_name' => 'Halo Halo', 'last_name' => 'Bandung'],
            ['first_name' => 'Hura Hura', 'last_name' => 'Bandung'],
            ['first_name' => 'Hali Helo', 'last_name' => 'Bundang'],
            ['first_name' => 'Helo Hali', 'last_name' => 'Bandang']
        ];

        $sortedName = $sn->sortArray($unsortedSplittedName);

        $this->assertEquals('Helo Hali', $sortedName[0]['first_name']);
        $this->assertEquals('Bandang', $sortedName[0]['last_name']);

        $this->assertEquals('Halo Halo', $sortedName[1]['first_name']);
        $this->assertEquals('Bandung', $sortedName[1]['last_name']);

        $this->assertEquals('Hura Hura', $sortedName[2]['first_name']);
        $this->assertEquals('Bandung', $sortedName[2]['last_name']);

        $this->assertEquals('Hali Helo', $sortedName[3]['first_name']);
        $this->assertEquals('Bundang', $sortedName[3]['last_name']);
    }
}
