<?php

class Sortname
{
	public function readFile(string $filename)
	{
		$array = [];
		$fp = @fopen($filename, 'r');

		if ($fp) {
			$array = explode("\n", fread($fp, filesize($filename)));
		}
		return $array;
	}

	function writeFile(array $datas, string $outputFilename)
	{
		$fp = fopen($outputFilename, "wb");
		$temp = "";
		foreach ($datas as $data) {
			if ($temp == "") {
				fwrite($fp, trim($data));
			} else {
				fwrite($fp, PHP_EOL . trim($data));
			}

			$temp = $data;
		}
		fclose($fp);
	}

	function splitName($name)
	{
		$name = trim($name);
		$explodedName = explode(" ", $name);
		$last_name = $explodedName[sizeof($explodedName) - 1];
		$first_name = str_replace($last_name, "", $name);
		return array('first_name' => trim($first_name), 'last_name' => trim($last_name));
	}

	function sortArray(array $unsortedSplittedArray)
	{
		array_multisort(
			array_column($unsortedSplittedArray, 'last_name'),
			SORT_ASC,
			array_column($unsortedSplittedArray, 'first_name'),
			SORT_ASC,
			$unsortedSplittedArray
		);

		return $unsortedSplittedArray;
	}
}
